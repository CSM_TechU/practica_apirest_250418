console.log("Hola Mundo!");
var express = require ('express');
var app = express();
var port = process.env.PORT || 3000;
var bodyParser = require('body-parser');
app.use(bodyParser.json());

app.listen(port);
console.log("API molona escuchando en "+port);

app.get("/apitechu/v1",
  function(req,res){
    console.log("GET /apitechu/v1");
    res.send('{"msg":"Hola desde API TechU"}');
  }
);

app.get("/apitechu/v1/users",
    function(req,res){
      console.log("GET /apitechu/v1/users");
      //res.sendFile('./usuarios.json');//DEPRECATED
      //res.sendFile ('usuarios.json',{root:__dirname});
      var users = require('./usuarios.json');
      res.send(users);
    }
);

app.post("/apitechu/v1/users",
    function(req,res){
      console.log("POST /apitechu/v1/users");
      console.log("first_name is "+req.body.first_name);
      console.log("last_name is "+req.body.last_name);
      console.log("country is "+req.body.country);

      var newUser = {
        "first_name":req.body.first_name,
        "last_name" : req.body.last_name,
        "country":req.body.country
      };

      var users = require('./usuarios.json'); //array
      users.push(newUser);
      writeUserDataToFile(users);
      console.log("Usuario cargado con éxito");
      res.send({"msg":"Usuario guardado con éxito"});

      //console.log("Usuario añadido con éxito");

      //var fs = require('fs');
      //var jsonUserData = JSON.stringify(users);

      //fs.writeFile("./usuarios.json",jsonUserData, "utf8",
      //  function(err){
      //    if(err){
      //        var msg="Error al escribir fichero de usuarios";
      //        console.log(msg);
      //      } else{
      //        var msg="Usuario persistido";
      //        console.log(msg);
      //      }
      //      res.send({"msg":msg});

      //    }
      //  );
      //res.send(users); //envia los datos a Postman
    }
);

app.delete("/apitechu/v1/users/:id",
  function(req,res){
    console.log("DELETE /apitechu/v1/users/:id");
    console.log(req.params);
    console.log(req.params.id);
    var users=require('./usuarios.json');
    users.splice(req.params.id - 1,1); // borra un elemento del array, el que ocupa la posición id-1
    writeUserDataToFile(users); //persistimos el array
    console.log("Usuario borrado");
    res.send({"msg":"Usuario borrado"});
  }
);

function writeUserDataToFile (data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json",jsonUserData, "utf8",
    function(err){
      if(err){
          //var msg="Error al escribir fichero de usuarios";
          console.log(err);
        } else{
          //var msg="Usuario persistido";
          console.log("Datos escritos en archivo");
        }
      }
    );
}

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req,res){
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Body");
    console.log(req.body);

    console.log("Headers");
    console.log(req.headers);
  }
);

//app.get("/apitechu/v1/login",
//    function(req,res){
//      console.log("GET /apitechu/v1/login");
//      //console.log("id " + req.body.id);
      //console.log("email "+req.body.email);
      //console.log("password "+req.body.password);
      //var users = require('./usuarios.json');
      //res.send(users);
  //}
  //  );

app.post("/apitechu/v1/login",
    function(req,res){
      console.log("POST /apitechu/v1/login");
      console.log("email "+req.body.email);
      console.log("password "+req.body.password);

      var users = require('./usuarios.json'); //array
      var userIsLogged = false;
      for (user of users){
        if(user.email == req.body.email && user.password == req.body.password){
          user.logged =true;
          userIsLogged=true;
          writeUserDataToFile(users); //persistimos el array
          break;
        }
      }
      if (userIsLogged == true){
        res.send("User " + user.id + " Logged:" + user.logged );
      }
      else {
        res.send("Login incorrecto");
      }
    }
);

app.post("/apitechu/v1/logout",
    function(req,res){
      console.log("POST /apitechu/v1/logout");
      var users = require('./usuarios.json'); //array
      var userLoggedOut = false;


      for (user of users){
        console.log("user.id " + user.id);
        console.log("req.body.id " + req.body.id);
        console.log(user.logged);
        if(user.id == req.body.id && user.logged==true){
            console.log("Logout correcto");
            //res.send("Logout correcto");
            userLoggedOut = true;
            delete user.logged;
          break;
        }
      }
      if (userLoggedOut == true){
        res.send("Logout correcto" );
      }
      else {
        res.send("Login incorrecto");
      }
    }
);
